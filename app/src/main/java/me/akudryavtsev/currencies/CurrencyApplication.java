package me.akudryavtsev.currencies;

import android.app.Application;

import me.akudryavtsev.currencies.di.ApplicationComponent;
import me.akudryavtsev.currencies.di.DaggerApplicationComponent;
import me.akudryavtsev.currencies.di.NetworkModule;

/**
 * @author Andrey Kudryavtsev on 2020-01-26.
 */
public class CurrencyApplication extends Application {

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationComponent = DaggerApplicationComponent
                .builder()
                .networkModule(new NetworkModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }
}
