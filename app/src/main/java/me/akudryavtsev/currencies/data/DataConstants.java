package me.akudryavtsev.currencies.data;

/**
 * @author Andrey Kudryavtsev on 2020-01-26.
 */
public interface DataConstants {
    String API_BASE_URL = "https://revolut.duckdns.org/";
}
