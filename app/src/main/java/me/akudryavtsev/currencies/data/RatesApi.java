package me.akudryavtsev.currencies.data;

import io.reactivex.Single;
import me.akudryavtsev.currencies.models.data.Currency;
import me.akudryavtsev.currencies.models.data.RatesResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author Andrey Kudryavtsev on 2020-01-26.
 */
public interface RatesApi {
    @GET("latest")
    Single<RatesResponse> getLatestRates(@Query("base") Currency base);
}
