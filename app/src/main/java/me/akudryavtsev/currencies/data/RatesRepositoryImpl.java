package me.akudryavtsev.currencies.data;

import androidx.annotation.NonNull;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import me.akudryavtsev.currencies.data.cache.CacheManager;
import me.akudryavtsev.currencies.domain.RatesRepository;
import me.akudryavtsev.currencies.models.data.Currency;
import me.akudryavtsev.currencies.models.data.RatesResponse;
import me.akudryavtsev.currencies.rx.RxSchedulers;
import me.akudryavtsev.currencies.utils.NetworkUtils;

/**
 * @author Andrey Kudryavtsev on 2020-01-26.
 */
public class RatesRepositoryImpl implements RatesRepository {

    private final RatesApi mRatesApi;
    private final CacheManager<Currency, RatesResponse> mRatesCacheManager;
    private final NetworkUtils mNetworkUtils;
    private final RxSchedulers mRxSchedulers;

    public RatesRepositoryImpl(@NonNull RatesApi ratesApi,
                               @NonNull CacheManager<Currency, RatesResponse> ratesCacheManager,
                               @NonNull NetworkUtils networkUtils,
                               @NonNull RxSchedulers rxSchedulers) {
        mRatesApi = ratesApi;
        mRatesCacheManager = ratesCacheManager;
        mNetworkUtils = networkUtils;
        mRxSchedulers = rxSchedulers;
    }

    @Override
    public Flowable<RatesResponse> getRates(@NonNull Currency base, int refreshPeriodInSec) {
        return Flowable.interval(0, refreshPeriodInSec, TimeUnit.SECONDS, mRxSchedulers.getIoScheduler())
                .onBackpressureLatest()
                .throttleLatest(refreshPeriodInSec, TimeUnit.SECONDS, mRxSchedulers.getIoScheduler())
                .flatMap(ignored ->
                        mRatesCacheManager.get(base)
                                .concatWith(getLatestFromApi(base))
                                .switchIfEmpty(Flowable.error(new NoSuchElementException())));
    }

    private Maybe<RatesResponse> getLatestFromApi(@NonNull Currency base) {
        return mNetworkUtils.checkNetwork()
                .andThen(mRatesApi.getLatestRates(base)
                        .doOnSuccess(response -> mRatesCacheManager.put(base, response)))
                .toMaybe()
                .onErrorResumeNext(Maybe.empty());
    }
}
