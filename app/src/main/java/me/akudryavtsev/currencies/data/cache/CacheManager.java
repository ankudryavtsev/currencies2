package me.akudryavtsev.currencies.data.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.reactivex.Maybe;

/**
 * @author Andrey Kudryavtsev on 2020-02-09.
 */
public class CacheManager<K, V> {

    private final Map<K, V> mCacheMap = new ConcurrentHashMap<>();

    public synchronized Maybe<V> get(K key) {
        return mCacheMap.containsKey(key) ? Maybe.fromCallable(() -> mCacheMap.get(key)) : Maybe.empty();
    }

    public synchronized void put(K key, V value) {
        mCacheMap.put(key, value);
    }
}
