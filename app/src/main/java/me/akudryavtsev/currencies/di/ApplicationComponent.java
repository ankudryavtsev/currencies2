package me.akudryavtsev.currencies.di;

import javax.inject.Singleton;

import dagger.Component;
import me.akudryavtsev.currencies.domain.RatesInteractor;
import me.akudryavtsev.currencies.rx.RxSchedulers;

/**
 * @author Andrey Kudryavtsev on 2020-01-26.
 */
@Singleton
@Component(modules = {CoreModule.class, NetworkModule.class, RatesModule.class})
public interface ApplicationComponent {
    RatesInteractor getRatesInteractor();

    RxSchedulers getRxSchedulers();
}
