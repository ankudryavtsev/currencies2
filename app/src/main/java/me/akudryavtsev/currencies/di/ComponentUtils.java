package me.akudryavtsev.currencies.di;

import android.content.Context;

import androidx.annotation.NonNull;

import me.akudryavtsev.currencies.CurrencyApplication;

/**
 * @author Andrey Kudryavtsev on 2020-01-26.
 */
public class ComponentUtils {
    public static ApplicationComponent getApplicationComponent(@NonNull Context context) {
        return ((CurrencyApplication) context.getApplicationContext()).getApplicationComponent();
    }
}
