package me.akudryavtsev.currencies.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.akudryavtsev.currencies.rx.RxSchedulers;
import me.akudryavtsev.currencies.rx.RxSchedulersImpl;

/**
 * @author Andrey Kudryavtsev on 2020-01-28.
 */
@Module
class CoreModule {
    @Singleton
    @Provides
    RxSchedulers provideSchedulers() {
        return new RxSchedulersImpl();
    }
}
