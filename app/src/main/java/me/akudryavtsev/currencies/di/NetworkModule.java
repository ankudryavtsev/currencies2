package me.akudryavtsev.currencies.di;

import android.content.Context;

import androidx.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.akudryavtsev.currencies.data.RatesApi;
import me.akudryavtsev.currencies.utils.NetworkUtils;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static me.akudryavtsev.currencies.data.DataConstants.API_BASE_URL;

/**
 * @author Andrey Kudryavtsev on 2020-01-26.
 */
@Module
public class NetworkModule {

    private final Context mContext;

    public NetworkModule(@NonNull Context context) {
        mContext = context;
    }

    @Singleton
    @Provides
    RatesApi provideRatesApi() {
        final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        final OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        return new Retrofit.Builder()
                .client(client)
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(RatesApi.class);
    }

    @Singleton
    @Provides
    NetworkUtils provideNetworkUtils() {
        return new NetworkUtils(mContext);
    }
}
