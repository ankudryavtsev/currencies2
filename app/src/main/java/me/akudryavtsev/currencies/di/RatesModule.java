package me.akudryavtsev.currencies.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.akudryavtsev.currencies.data.RatesApi;
import me.akudryavtsev.currencies.data.RatesRepositoryImpl;
import me.akudryavtsev.currencies.data.cache.CacheManager;
import me.akudryavtsev.currencies.domain.RatesInteractor;
import me.akudryavtsev.currencies.domain.RatesRepository;
import me.akudryavtsev.currencies.rx.RxSchedulers;
import me.akudryavtsev.currencies.utils.NetworkUtils;

/**
 * @author Andrey Kudryavtsev on 2020-01-28.
 */
@Module
class RatesModule {
    @Singleton
    @Provides
    RatesInteractor provideRatesInteractor(RatesRepository ratesRepository) {
        return new RatesInteractor(ratesRepository);
    }

    @Singleton
    @Provides
    RatesRepository provideRatesRepository(RatesApi ratesApi, NetworkUtils networkUtils, RxSchedulers rxSchedulers) {
        return new RatesRepositoryImpl(ratesApi, new CacheManager<>(), networkUtils, rxSchedulers);
    }
}
