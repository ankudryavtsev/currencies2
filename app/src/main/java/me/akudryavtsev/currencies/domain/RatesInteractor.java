package me.akudryavtsev.currencies.domain;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import me.akudryavtsev.currencies.models.data.Currency;
import me.akudryavtsev.currencies.models.presentation.AmountWithCurrency;

/**
 * @author Andrey Kudryavtsev on 2020-01-27.
 */
public class RatesInteractor {

    private static final AmountWithCurrency DEFAULT_AMOUNT_WITH_CURRENCY = new AmountWithCurrency(Currency.EUR, 100.0);
    private static final int REFRESH_PERIOD_IN_SEC = 1;

    private final RatesRepository mRatesRepository;

    public RatesInteractor(@NonNull RatesRepository ratesRepository) {
        mRatesRepository = ratesRepository;
    }

    public Flowable<List<AmountWithCurrency>> getAmountWithCurrencies(@Nullable AmountWithCurrency amountWithCurrency) {
        final AmountWithCurrency base = amountWithCurrency == null ? DEFAULT_AMOUNT_WITH_CURRENCY : amountWithCurrency;
        return mRatesRepository.getRates(base.getCurrency(), REFRESH_PERIOD_IN_SEC)
                .map(ratesResponse -> calculateList(ratesResponse.getRates(), base));
    }

    private List<AmountWithCurrency> calculateList(@NonNull Map<Currency, Double> rates, @NonNull AmountWithCurrency base) {
        final List<AmountWithCurrency> result = new ArrayList<>(rates.size() + 1);
        result.add(base);
        for (Map.Entry<Currency, Double> entry : rates.entrySet()) {
            result.add(new AmountWithCurrency(entry.getKey(), entry.getValue() * base.getAmount()));
        }
        return result;
    }
}
