package me.akudryavtsev.currencies.domain;

import androidx.annotation.NonNull;

import io.reactivex.Flowable;
import me.akudryavtsev.currencies.models.data.Currency;
import me.akudryavtsev.currencies.models.data.RatesResponse;

/**
 * @author Andrey Kudryavtsev on 2020-01-26.
 */
public interface RatesRepository {
    Flowable<RatesResponse> getRates(@NonNull Currency base, int refreshPeriodInSec);
}
