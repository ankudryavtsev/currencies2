package me.akudryavtsev.currencies.models.data;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

import me.akudryavtsev.currencies.R;

/**
 * @author Andrey Kudryavtsev on 2020-01-26.
 */
public enum Currency {
    AUD, BGN, BRL, CAD(R.drawable.ic_canada, R.string.cad), CHF, CNY,
    CZK, DKK, EUR, GBP, HKD, HRK,
    HUF, IDR, ILS, INR, ISK, JPY,
    KRW, MXN, MYR, NOK, NZD, PHP,
    PLN, RON, RUB(R.drawable.ic_russia, R.string.rus), SEK, SGD, THB,
    TRY, USD(R.drawable.ic_united_states_of_america, R.string.usd), ZAR;

    @DrawableRes
    private final int mIconRes;
    @StringRes
    private final int mDescriptionRes;

    Currency(@DrawableRes int iconRes, @StringRes int descriptionRes) {
        mIconRes = iconRes;
        mDescriptionRes = descriptionRes;
    }

    Currency() {
        mIconRes = R.drawable.ic_united_states_of_america;
        mDescriptionRes = R.string.usd;
    }

    public int getIconRes() {
        return mIconRes;
    }

    public int getDescriptionRes() {
        return mDescriptionRes;
    }
}
