package me.akudryavtsev.currencies.models.data;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

/**
 * @author Andrey Kudryavtsev on 2020-01-26.
 */
public class RatesResponse {
    @SerializedName("base")
    private Currency base;

    @SerializedName("rates")
    private Map<Currency, Double> rates;

    @NonNull
    public Currency getBase() {
        return base;
    }

    public void setBase(Currency base) {
        this.base = base;
    }

    @NonNull
    public Map<Currency, Double> getRates() {
        return rates;
    }

    public void setRates(Map<Currency, Double> rates) {
        this.rates = rates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RatesResponse that = (RatesResponse) o;

        if (base != that.base) {
            return false;
        }
        return rates.equals(that.rates);
    }

    @Override
    public int hashCode() {
        int result = base.hashCode();
        result = 31 * result + rates.hashCode();
        return result;
    }
}
