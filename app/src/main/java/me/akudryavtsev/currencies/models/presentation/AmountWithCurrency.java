package me.akudryavtsev.currencies.models.presentation;

import androidx.annotation.NonNull;

import me.akudryavtsev.currencies.models.data.Currency;

/**
 * @author Andrey Kudryavtsev on 2020-01-26.
 */
public class AmountWithCurrency {
    private final Currency mCurrency;

    private double mAmount;

    public AmountWithCurrency(Currency currency, double amount) {
        mCurrency = currency;
        mAmount = amount;
    }


    public Currency getCurrency() {
        return mCurrency;
    }

    public double getAmount() {
        return mAmount;
    }

    public void setAmount(double amount) {
        mAmount = amount;
    }

    @NonNull
    @Override
    public String toString() {
        return "AmountWithCurrency{" +
                "mCurrency=" + mCurrency +
                ", mAmount=" + mAmount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AmountWithCurrency that = (AmountWithCurrency) o;

        if (Double.compare(that.mAmount, mAmount) != 0) {
            return false;
        }
        return mCurrency == that.mCurrency;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = mCurrency != null ? mCurrency.hashCode() : 0;
        temp = Double.doubleToLongBits(mAmount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
