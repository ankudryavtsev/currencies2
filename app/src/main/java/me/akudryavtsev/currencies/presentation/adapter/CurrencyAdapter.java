package me.akudryavtsev.currencies.presentation.adapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import me.akudryavtsev.currencies.R;
import me.akudryavtsev.currencies.models.data.Currency;
import me.akudryavtsev.currencies.models.presentation.AmountWithCurrency;
import me.akudryavtsev.currencies.presentation.viewmodel.BaseCurrencyChangedListener;

/**
 * @author Andrey Kudryavtsev on 2020-01-26.
 */
public class CurrencyAdapter extends ListAdapter<AmountWithCurrency, CurrencyAdapter.ViewHolder> implements ItemInteractionListener {

    private final BaseCurrencyChangedListener mBaseCurrencyChangedListener;

    public CurrencyAdapter(@NonNull BaseCurrencyChangedListener baseCurrencyChangedListener) {
        super(new DiffCallback());
        mBaseCurrencyChangedListener = baseCurrencyChangedListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        final View view = layoutInflater.inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindView(getItem(position));
    }

    @Override
    public void onItemSelected(int position) {
        if (position > 0) {
            final List<AmountWithCurrency> currentList = new ArrayList<>(getCurrentList());
            final AmountWithCurrency amountWithCurrency = currentList.get(position);
            currentList.remove(position);
            currentList.add(0, amountWithCurrency);
            submitList(currentList);
            mBaseCurrencyChangedListener.onBaseCurrencyChanged(amountWithCurrency);
        }
    }

    @Override
    public void onBaseItemValueChanged(double newValue) {
        final AmountWithCurrency base = getItem(0);
        base.setAmount(newValue);
        mBaseCurrencyChangedListener.onBaseCurrencyChanged(base);
    }

    @Override
    public void submitList(@Nullable List<AmountWithCurrency> list) {
        if (list != null) {
            final AmountWithCurrency base = list.remove(0);
            Collections.sort(list, (a1, a2) -> a1.getCurrency().compareTo(a2.getCurrency()));
            list.add(0, base);
        }
        super.submitList(list);
    }

    @Nullable
    public AmountWithCurrency getBaseCurrency() {
        return getItemCount() > 0 ? getItem(0) : null;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final static DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.00");

        private final TextView mTitleView;
        private final TextView mSubtitleView;
        private final ImageView mIconView;
        private final EditText mAmountView;

        @DrawableRes
        private int mIconRes;

        private ViewHolder(@NonNull View itemView, @NonNull ItemInteractionListener itemInteractionListener) {
            super(itemView);
            mTitleView = itemView.findViewById(R.id.title_view);
            mSubtitleView = itemView.findViewById(R.id.subtitle_view);
            mIconView = itemView.findViewById(R.id.icon_view);
            mAmountView = itemView.findViewById(R.id.amount_view);
            mAmountView.setOnFocusChangeListener((v, hasFocus) -> {
                if (hasFocus) {
                    itemInteractionListener.onItemSelected(getAdapterPosition());
                }
            });
            mAmountView.addTextChangedListener(new AmountTextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() != 0 && getAdapterPosition() == 0) {
                        itemInteractionListener.onBaseItemValueChanged(Double.parseDouble(s.toString()));
                    }
                }
            });
        }

        private void bindView(@NonNull AmountWithCurrency amountWithCurrency) {
            final Currency currency = amountWithCurrency.getCurrency();
            mTitleView.setText(currency.name());
            mSubtitleView.setText(currency.getDescriptionRes());
            if (currency.getIconRes() != mIconRes) {
                mIconView.setImageResource(currency.getIconRes());
                mIconRes = currency.getIconRes();
            }
            mAmountView.setText(DECIMAL_FORMAT.format(amountWithCurrency.getAmount()));
        }

        private abstract static class AmountTextWatcher implements TextWatcher {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Empty
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Empty
            }
        }
    }

    static class DiffCallback extends DiffUtil.ItemCallback<AmountWithCurrency> {

        @Override
        public boolean areItemsTheSame(@NonNull AmountWithCurrency oldItem, @NonNull AmountWithCurrency newItem) {
            return oldItem.getCurrency() == newItem.getCurrency();
        }

        @Override
        public boolean areContentsTheSame(@NonNull AmountWithCurrency oldItem, @NonNull AmountWithCurrency newItem) {
            return oldItem.equals(newItem);
        }
    }
}
