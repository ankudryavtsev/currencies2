package me.akudryavtsev.currencies.presentation.adapter;

/**
 * @author Andrey Kudryavtsev on 2020-01-29.
 */
public interface ItemInteractionListener {
    void onItemSelected(int position);

    void onBaseItemValueChanged(double newValue);
}
