package me.akudryavtsev.currencies.presentation.view;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import me.akudryavtsev.currencies.R;
import me.akudryavtsev.currencies.di.ApplicationComponent;
import me.akudryavtsev.currencies.di.ComponentUtils;
import me.akudryavtsev.currencies.presentation.adapter.CurrencyAdapter;
import me.akudryavtsev.currencies.presentation.viewmodel.MainViewModel;

public class MainActivity extends AppCompatActivity {

    private CurrencyAdapter mCurrencyAdapter;
    private MainViewModel mMainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupToolbar();
        mMainViewModel = createViewModel();
        setupRecyclerView(mMainViewModel);
        mMainViewModel.getListLiveData().observe(this, amountWithCurrencies -> mCurrencyAdapter.submitList(amountWithCurrencies));
        mMainViewModel.getErrorLiveData().observe(this, this::showErrorDialog);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMainViewModel.loadCurrencies(mCurrencyAdapter.getBaseCurrency());
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMainViewModel.stopLoading();
    }

    private void showErrorDialog(@StringRes int errorStringRes) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.error)
                .setMessage(errorStringRes)
                .setNeutralButton(android.R.string.ok, null)
                .show();
    }

    private MainViewModel createViewModel() {
        return new ViewModelProvider(this, new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                final ApplicationComponent applicationComponent = ComponentUtils.getApplicationComponent(MainActivity.this);
                return (T) new MainViewModel(applicationComponent.getRatesInteractor(), applicationComponent.getRxSchedulers());
            }
        }).get(MainViewModel.class);
    }

    private void setupRecyclerView(@NonNull MainViewModel mainViewModel) {
        final RecyclerView recyclerView = findViewById(R.id.recycler_view);
        mCurrencyAdapter = new CurrencyAdapter(mainViewModel);
        recyclerView.setAdapter(mCurrencyAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    private void setupToolbar() {
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }
}
