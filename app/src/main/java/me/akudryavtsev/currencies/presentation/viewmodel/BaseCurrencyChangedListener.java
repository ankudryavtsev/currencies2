package me.akudryavtsev.currencies.presentation.viewmodel;

import androidx.annotation.NonNull;

import me.akudryavtsev.currencies.models.presentation.AmountWithCurrency;

/**
 * @author Andrey Kudryavtsev on 2020-01-29.
 */
public interface BaseCurrencyChangedListener {
    void onBaseCurrencyChanged(@NonNull AmountWithCurrency amountWithCurrency);
}
