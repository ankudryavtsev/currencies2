package me.akudryavtsev.currencies.presentation.viewmodel;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;
import java.util.NoSuchElementException;

import io.reactivex.disposables.Disposable;
import me.akudryavtsev.currencies.R;
import me.akudryavtsev.currencies.domain.RatesInteractor;
import me.akudryavtsev.currencies.models.presentation.AmountWithCurrency;
import me.akudryavtsev.currencies.presentation.SingleLiveEvent;
import me.akudryavtsev.currencies.rx.RxSchedulers;

/**
 * @author Andrey Kudryavtsev on 2020-01-26.
 */
public class MainViewModel extends ViewModel implements BaseCurrencyChangedListener {

    private static final String TAG = "MainViewModel";

    private MutableLiveData<List<AmountWithCurrency>> mListLiveData = new MutableLiveData<>();
    private SingleLiveEvent<Integer> mErrorLiveData = new SingleLiveEvent<>();

    private final RatesInteractor mRatesInteractor;
    private final RxSchedulers mRxSchedulers;

    private Disposable mDisposable;

    public MainViewModel(@NonNull RatesInteractor ratesInteractor,
                         @NonNull RxSchedulers rxSchedulers) {
        mRatesInteractor = ratesInteractor;
        mRxSchedulers = rxSchedulers;
    }

    @Override
    public void onBaseCurrencyChanged(@NonNull AmountWithCurrency base) {
        loadCurrencies(base);
    }

    @Override
    protected void onCleared() {
        dispose();
    }

    public void loadCurrencies(@Nullable AmountWithCurrency amountWithCurrency) {
        dispose();
        mDisposable = mRatesInteractor.getAmountWithCurrencies(amountWithCurrency)
                .subscribeOn(mRxSchedulers.getIoScheduler())
                .observeOn(mRxSchedulers.getMainThreadScheduler())
                .subscribe(amountWithCurrencies -> mListLiveData.setValue(amountWithCurrencies), this::onError);

    }

    public void stopLoading() {
        dispose();
    }

    public LiveData<List<AmountWithCurrency>> getListLiveData() {
        return mListLiveData;
    }

    public LiveData<Integer> getErrorLiveData() {
        return mErrorLiveData;
    }

    private void dispose() {
        if (mDisposable != null) {
            mDisposable.dispose();
        }
    }

    private void onError(@NonNull Throwable throwable) {
        Log.e(TAG, "onError: " + throwable.getMessage(), throwable);
        if (throwable instanceof NoSuchElementException) {
            mErrorLiveData.postValue(R.string.network_is_required);
        }
    }
}
