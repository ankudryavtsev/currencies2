package me.akudryavtsev.currencies.rx;

import io.reactivex.Scheduler;

/**
 * @author Andrey Kudryavtsev on 2020-01-28.
 */
public interface RxSchedulers {
    Scheduler getIoScheduler();

    Scheduler getMainThreadScheduler();
}
