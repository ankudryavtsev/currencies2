package me.akudryavtsev.currencies.utils;

import android.content.Context;
import android.net.ConnectivityManager;

import androidx.annotation.NonNull;

import io.reactivex.Completable;

/**
 * @author Andrey Kudryavtsev on 2020-02-09.
 */
public class NetworkUtils {

    private final Context mContext;

    public NetworkUtils(@NonNull Context context) {
        mContext = context;
    }

    /**
     * Проверка наличия подключения
     * <p>При отсутствии подключения бросает {@link NoNetworkException}</p>
     */
    public Completable checkNetwork() {
        return Completable.fromAction(() -> {
            ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

            final boolean isConnected = cm != null && cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
            if (!isConnected) {
                throw new NoNetworkException();
            }
        });

    }
}
