package me.akudryavtsev.currencies.utils;

/**
 * @author Andrey Kudryavtsev on 2020-02-09.
 */
class NoNetworkException extends RuntimeException {
    NoNetworkException() {
        super("No Internet");
    }
}
