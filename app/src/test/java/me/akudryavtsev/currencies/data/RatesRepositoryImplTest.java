package me.akudryavtsev.currencies.data;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.subscribers.TestSubscriber;
import me.akudryavtsev.currencies.data.cache.CacheManager;
import me.akudryavtsev.currencies.models.data.Currency;
import me.akudryavtsev.currencies.models.data.RatesResponse;
import me.akudryavtsev.currencies.rx.RxSchedulersTest;
import me.akudryavtsev.currencies.utils.NetworkUtils;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Andrey Kudryavtsev on 2020-02-10.
 */
@RunWith(MockitoJUnitRunner.class)
public class RatesRepositoryImplTest {
    private RatesRepositoryImpl mRatesRepository;

    @Mock
    private RatesApi mRatesApi;
    @Mock
    private CacheManager<Currency, RatesResponse> mRatesResponseCacheManager;
    @Mock
    private NetworkUtils mNetworkUtils;

    private RxSchedulersTest mRxSchedulers = new RxSchedulersTest();

    @Before
    public void setUp() {
        mRxSchedulers = new RxSchedulersTest();
        mRatesRepository = new RatesRepositoryImpl(mRatesApi, mRatesResponseCacheManager, mNetworkUtils, mRxSchedulers);
    }

    @Test
    public void getRatesFromNetwork() {
        final Currency currency = Currency.EUR;
        when(mRatesResponseCacheManager.get(currency)).thenReturn(Maybe.empty());
        when(mNetworkUtils.checkNetwork()).thenReturn(Completable.complete());
        final RatesResponse ratesResponse = mock(RatesResponse.class);
        when(mRatesApi.getLatestRates(currency)).thenReturn(Single.just(ratesResponse));
        final TestSubscriber<RatesResponse> testSubscriber = mRatesRepository.getRates(currency, 1).test();
        mRxSchedulers.getTestScheduler().advanceTimeBy(500, TimeUnit.MILLISECONDS);
        testSubscriber.assertValue(ratesResponse);
    }

    @Test
    public void getRatesFromCacheAndNetwork() {
        final Currency currency = Currency.EUR;
        final RatesResponse ratesResponseFromCache = mock(RatesResponse.class);
        final RatesResponse ratesResponseFromNetwork = mock(RatesResponse.class);
        when(mRatesResponseCacheManager.get(currency)).thenReturn(Maybe.just(ratesResponseFromCache));
        when(mNetworkUtils.checkNetwork()).thenReturn(Completable.complete());
        when(mRatesApi.getLatestRates(currency)).thenReturn(Single.just(ratesResponseFromNetwork));
        final TestSubscriber<RatesResponse> testSubscriber = mRatesRepository.getRates(currency, 1).test();
        mRxSchedulers.getTestScheduler().advanceTimeBy(500, TimeUnit.MILLISECONDS);
        testSubscriber.assertValues(ratesResponseFromCache, ratesResponseFromNetwork);
    }

    @Test
    public void getRatesFromCache() {
        final Currency currency = Currency.EUR;
        final RatesResponse ratesResponseFromCache = mock(RatesResponse.class);
        when(mRatesResponseCacheManager.get(currency)).thenReturn(Maybe.just(ratesResponseFromCache));
        when(mNetworkUtils.checkNetwork()).thenReturn(Completable.complete());
        when(mRatesApi.getLatestRates(currency)).thenReturn(Single.error(new RuntimeException()));
        final TestSubscriber<RatesResponse> testSubscriber = mRatesRepository.getRates(currency, 1).test();
        mRxSchedulers.getTestScheduler().advanceTimeBy(500, TimeUnit.MILLISECONDS);
        testSubscriber.assertValues(ratesResponseFromCache);
    }

    @Test
    public void getRatesNoCache() {
        final Currency currency = Currency.EUR;
        when(mRatesResponseCacheManager.get(currency)).thenReturn(Maybe.empty());
        when(mNetworkUtils.checkNetwork()).thenReturn(Completable.complete());
        when(mRatesApi.getLatestRates(currency)).thenReturn(Single.error(new RuntimeException()));
        final TestSubscriber<RatesResponse> testSubscriber = mRatesRepository.getRates(currency, 1).test();
        mRxSchedulers.getTestScheduler().advanceTimeBy(500, TimeUnit.MILLISECONDS);
        testSubscriber.assertError(NoSuchElementException.class);
    }
}