package me.akudryavtsev.currencies.domain;

import androidx.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import me.akudryavtsev.currencies.models.data.Currency;
import me.akudryavtsev.currencies.models.data.RatesResponse;
import me.akudryavtsev.currencies.models.presentation.AmountWithCurrency;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Andrey Kudryavtsev on 2020-02-11.
 */
@RunWith(MockitoJUnitRunner.class)
public class RatesInteractorTest {

    private static final AmountWithCurrency DEFAULT_AMOUNT_WITH_CURRENCY = new AmountWithCurrency(Currency.EUR, 100.0);
    private static final int REFRESH_PERIOD_IN_SEC = 1;

    private RatesInteractor mRatesInteractor;

    @Mock
    private RatesRepository mRatesRepository;

    @Before
    public void setUp() {
        mRatesInteractor = new RatesInteractor(mRatesRepository);
    }

    @Test
    public void getAmountWithCurrenciesDefault() {
        final RatesResponse ratesResponse = getRatesResponse();
        when(mRatesRepository.getRates(DEFAULT_AMOUNT_WITH_CURRENCY.getCurrency(), REFRESH_PERIOD_IN_SEC)).thenReturn(Flowable.just(ratesResponse));
        final List<AmountWithCurrency> expected = getExpectedList(DEFAULT_AMOUNT_WITH_CURRENCY);
        final List<AmountWithCurrency> actual = mRatesInteractor.getAmountWithCurrencies(null).blockingFirst();
        sort(expected, DEFAULT_AMOUNT_WITH_CURRENCY);
        sort(actual, DEFAULT_AMOUNT_WITH_CURRENCY);
        assertThat(actual, is(expected));
    }

    @Test
    public void getAmountWithCurrencies() {
        final AmountWithCurrency base = new AmountWithCurrency(Currency.CAD, 100);
        final RatesResponse ratesResponse = getRatesResponse();
        when(mRatesRepository.getRates(base.getCurrency(), REFRESH_PERIOD_IN_SEC)).thenReturn(Flowable.just(ratesResponse));
        final List<AmountWithCurrency> expected = getExpectedList(base);
        final List<AmountWithCurrency> actual = mRatesInteractor.getAmountWithCurrencies(base).blockingFirst();
        sort(expected, base);
        sort(actual, base);
        assertThat(actual, is(expected));
    }

    private void sort(@NonNull List<AmountWithCurrency> amountWithCurrencies, @NonNull AmountWithCurrency base) {
        Collections.sort(amountWithCurrencies, (first, second) -> {
            if (first.equals(base)) {
                return -1;
            } else if (second.equals(base)) {
                return 1;
            } else {
                return first.getCurrency().compareTo(second.getCurrency());
            }
        });
    }

    private RatesResponse getRatesResponse() {
        final RatesResponse ratesResponse = mock(RatesResponse.class);
        final Map<Currency, Double> rates = new HashMap<>();
        rates.put(Currency.AUD, 343d);
        rates.put(Currency.BGN, 3433d);
        rates.put(Currency.CNY, 3233d);
        when(ratesResponse.getRates()).thenReturn(rates);
        return ratesResponse;
    }

    private List<AmountWithCurrency> getExpectedList(@NonNull AmountWithCurrency base) {
        return Arrays.asList(base,
                new AmountWithCurrency(Currency.AUD, 34300d),
                new AmountWithCurrency(Currency.BGN, 343300d),
                new AmountWithCurrency(Currency.CNY, 323300d));
    }
}