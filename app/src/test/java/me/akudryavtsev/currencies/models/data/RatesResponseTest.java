package me.akudryavtsev.currencies.models.data;

import com.google.gson.Gson;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Andrey Kudryavtsev on 2020-01-26.
 */
public class RatesResponseTest {

    private static final String SOURCE = "{\"base\":\"EUR\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":1.6136,\"BGN\":1.9524,\"BRL\":4.7834," +
            "\"CAD\":1.5311,\"CHF\":1.1255,\"CNY\":7.9312,\"CZK\":25.67,\"DKK\":7.4437,\"GBP\":0.89667,\"HKD\":9.1164,\"HRK\":7.4211,\"HUF\":325.92," +
            "\"IDR\":17293.0,\"ILS\":4.1633,\"INR\":83.571,\"ISK\":127.58,\"JPY\":129.32,\"KRW\":1302.5,\"MXN\":22.326,\"MYR\":4.8036,\"NOK\":9.7589," +
            "\"NZD\":1.7602,\"PHP\":62.483,\"PLN\":4.3107,\"RON\":4.6304,\"RUB\":79.436,\"SEK\":10.572,\"SGD\":1.5972,\"THB\":38.063,\"TRY\":7.6149," +
            "\"USD\":1.1614,\"ZAR\":17.792}}";

    @Test
    public void parse() {
        final Gson gson = new Gson();
        final RatesResponse ratesResponse = gson.fromJson(SOURCE, RatesResponse.class);
        assertEquals(Currency.EUR, ratesResponse.getBase());
        assertNotNull(ratesResponse.getRates());
    }
}