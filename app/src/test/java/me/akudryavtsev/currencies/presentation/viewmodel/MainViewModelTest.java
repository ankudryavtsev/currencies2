package me.akudryavtsev.currencies.presentation.viewmodel;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.Observer;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import io.reactivex.Flowable;
import me.akudryavtsev.currencies.R;
import me.akudryavtsev.currencies.domain.RatesInteractor;
import me.akudryavtsev.currencies.models.data.Currency;
import me.akudryavtsev.currencies.models.presentation.AmountWithCurrency;
import me.akudryavtsev.currencies.rx.RxSchedulersStub;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Andrey Kudryavtsev on 2020-02-11.
 */
@RunWith(MockitoJUnitRunner.class)
public class MainViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Mock
    RatesInteractor mRatesInteractor;

    @Mock
    private Observer<List<AmountWithCurrency>> mListObserver;

    @Mock
    private Observer<Integer> mErrorObserver;

    private MainViewModel mMainViewModel;

    @Before
    public void setUp() {
        mMainViewModel = new MainViewModel(mRatesInteractor, new RxSchedulersStub());
        mMainViewModel.getListLiveData().observeForever(mListObserver);
        mMainViewModel.getErrorLiveData().observeForever(mErrorObserver);
    }

    @Test
    public void loadCurrencies() {
        final AmountWithCurrency amountWithCurrency = new AmountWithCurrency(Currency.CAD, 100.0);
        final List<AmountWithCurrency> expectedList = Arrays.asList(amountWithCurrency, new AmountWithCurrency(Currency.USD, 1));
        when(mRatesInteractor.getAmountWithCurrencies(amountWithCurrency))
                .thenReturn(Flowable.just(expectedList));
        mMainViewModel.loadCurrencies(amountWithCurrency);
        verify(mListObserver).onChanged(expectedList);
        verify(mErrorObserver, never()).onChanged(anyInt());
    }

    @Test
    public void onBaseCurrencyChanged() {
        final AmountWithCurrency amountWithCurrency = new AmountWithCurrency(Currency.CAD, 100.0);
        final List<AmountWithCurrency> expectedList = Arrays.asList(amountWithCurrency, new AmountWithCurrency(Currency.USD, 1));
        when(mRatesInteractor.getAmountWithCurrencies(amountWithCurrency))
                .thenReturn(Flowable.just(expectedList));
        mMainViewModel.onBaseCurrencyChanged(amountWithCurrency);
        verify(mListObserver).onChanged(expectedList);
        verify(mErrorObserver, never()).onChanged(anyInt());
    }

    @Test
    public void loadCurrenciesError() {
        final AmountWithCurrency amountWithCurrency = new AmountWithCurrency(Currency.CAD, 100.0);
        when(mRatesInteractor.getAmountWithCurrencies(amountWithCurrency))
                .thenReturn(Flowable.error(new NoSuchElementException()));
        mMainViewModel.loadCurrencies(amountWithCurrency);
        verify(mListObserver, never()).onChanged(anyList());
        verify(mErrorObserver).onChanged(R.string.network_is_required);
    }
}