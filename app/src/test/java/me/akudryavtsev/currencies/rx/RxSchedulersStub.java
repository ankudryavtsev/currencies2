package me.akudryavtsev.currencies.rx;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Andrey Kudryavtsev on 2020-02-11.
 */
public class RxSchedulersStub implements RxSchedulers {
    @Override
    public Scheduler getIoScheduler() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler getMainThreadScheduler() {
        return Schedulers.trampoline();
    }
}
