package me.akudryavtsev.currencies.rx;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.TestScheduler;

/**
 * @author Andrey Kudryavtsev on 2020-02-10.
 */
public class RxSchedulersTest implements RxSchedulers {

    private final TestScheduler mTestScheduler = new TestScheduler();

    @Override
    public Scheduler getIoScheduler() {
        return mTestScheduler;
    }

    @Override
    public Scheduler getMainThreadScheduler() {
        return mTestScheduler;
    }

    public TestScheduler getTestScheduler() {
        return mTestScheduler;
    }
}
